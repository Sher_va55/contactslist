package com.ppjava13v2.vera.listviewhome;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by User on 19.09.2015.
 */
public class CustomAdapter extends ArrayAdapter <String>  {

    private final Context context;
    private final List<String> values;

    public CustomAdapter(Context context, List<String> values) {
        super(context, R.layout.list_with_delete,values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
         LayoutInflater inflater =
                (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View currentView;

        currentView =
                convertView == null                                     ?
                        inflater.inflate(R.layout.list_with_delete, null)  :
                        convertView;

        TextView textView = (TextView) currentView.findViewById(R.id.sampleTextView);
        Button button = (Button) currentView.findViewById(R.id.sampleButton);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                deleteContact (position);
                notifyDataSetChanged();
            }
        });

        textView.setText(values.get(position));




        return currentView;
    }

    private void deleteContact (int position) {
        values.remove(position);
    }


}
