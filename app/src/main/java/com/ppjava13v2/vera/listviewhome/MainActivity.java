package com.ppjava13v2.vera.listviewhome;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.ppjava13v2.vera.listviewhome.Services.SaveDataService;

import java.util.List;


public class MainActivity extends Activity implements View.OnClickListener {

    private SaveDataService saveData;
    private ListView listView;
    private List<String> fioList;
    private CustomAdapter customAdapter;

    public MainActivity() {
        saveData=new SaveDataService();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button buttonAdd= (Button) findViewById(R.id.add);
        buttonAdd.setOnClickListener(this);
        initListView ();
    }

    private void initListView() {

        fioList=saveData.getFioUsers();
        listView = (ListView) findViewById(R.id.list);
        customAdapter = new CustomAdapter(this,fioList);
        listView.setAdapter(customAdapter);
    }

    @Override
    public void onClick(View v) {
        showRegisterDialog(customAdapter,fioList);

    }

    public void showRegisterDialog (CustomAdapter customAdapter,List<String> fioList) {

        DialogFragment registerDialog = new RegistrationDialog(saveData,customAdapter,fioList);
        registerDialog.setCancelable(false);
        registerDialog.show (getFragmentManager (),"registerTag");

    }
}
