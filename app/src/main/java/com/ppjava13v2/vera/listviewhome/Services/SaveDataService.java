package com.ppjava13v2.vera.listviewhome.Services;

import java.util.ArrayList;

/**
 * Manipulations user's data
 */
public class SaveDataService {

    private ArrayList <String> fioUsers;

    public SaveDataService() {

        fioUsers=new ArrayList<>();
    }

    public ArrayList<String> getFioUsers() {
        return fioUsers;
    }

    public void saveUser (String user) {
        fioUsers.add (user);
    }
}



