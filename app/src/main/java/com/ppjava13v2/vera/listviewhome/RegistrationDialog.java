package com.ppjava13v2.vera.listviewhome;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.ppjava13v2.vera.listviewhome.Services.SaveDataService;

import java.util.List;

/**
 * Creates dialog window for registration users
 */
public class RegistrationDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private View registerView;
    private SaveDataService saveData;
    private CustomAdapter customAdapter;
    private List<String> fioList;
    private EditText firstName;
    private EditText lastName;

    @SuppressLint("ValidFragment")
    public RegistrationDialog(SaveDataService saveData,CustomAdapter customAdapter,List<String> fioList) {
        this.saveData = saveData;
        this.customAdapter=customAdapter;
        this.fioList=fioList;
    }

    public RegistrationDialog() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        registerView =inflater.inflate (R.layout.register_layout,null);
        firstName=(EditText)registerView.findViewById(R.id.firstName);
        lastName=(EditText)registerView.findViewById(R.id.lastName);


        AlertDialog.Builder builder = new AlertDialog.Builder (getActivity());
        builder.setTitle("Enter User's data")
                .setCancelable(false)
                .setView(registerView)
                .setPositiveButton("Save",this)
                .setNegativeButton("No",this);


        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                clickPositiveButton ();
                break;

            case DialogInterface.BUTTON_NEGATIVE:
                break;
        }

    }

    public void clickPositiveButton () {

        String result=firstName.getText().toString()+" "+lastName.getText().toString();
        saveData.saveUser(result);
        fioList=saveData.getFioUsers();
        customAdapter.notifyDataSetChanged();

    }
}
